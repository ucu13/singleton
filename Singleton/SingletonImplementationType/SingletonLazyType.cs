﻿using System;

namespace Singleton.SingletonImplementationType
{
    public sealed class SingletonLazyType
    {
        private static readonly Lazy<SingletonLazyType> lazy = new Lazy<SingletonLazyType> (() => new SingletonLazyType());

        public static SingletonLazyType Instance { get { return lazy.Value; } }

        private SingletonLazyType()
        {
        }
    }
}