﻿
namespace Singleton.SingletonImplementationType
{
    public sealed class SingletonLazy
    {
        private SingletonLazy()
        {
        }

        public static SingletonLazy Instance { get { return Nested.instance; } }

        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly SingletonLazy instance = new SingletonLazy();
        }
    }
}