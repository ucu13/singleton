﻿
namespace Singleton.SingletonImplementationType
{
    public sealed class SingletonDoubleLockingCheckThreadSafe
    {
        private static SingletonDoubleLockingCheckThreadSafe instance = null;
        private static readonly object padlock = new object();

        private SingletonDoubleLockingCheckThreadSafe()
        {
        }

        public static SingletonDoubleLockingCheckThreadSafe Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new SingletonDoubleLockingCheckThreadSafe();
                        }
                    }
                }
                return instance;
            }
        }
    }
}