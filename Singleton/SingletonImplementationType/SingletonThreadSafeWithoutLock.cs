﻿
namespace Singleton.SingletonImplementationType
{
    public sealed class SingletonThreadSafeWithoutLock
    {
        static SingletonThreadSafeWithoutLock()
        {
        }

        private SingletonThreadSafeWithoutLock()
        {
        }

        public static SingletonThreadSafeWithoutLock Instance { get; } = new SingletonThreadSafeWithoutLock();
    }
}