﻿
namespace Singleton.SingletonImplementationType
{
    public sealed class SingletonSimpleThreadSafe
    {
        private static SingletonSimpleThreadSafe instance = null;
        private static readonly object padlock = new object();

        private SingletonSimpleThreadSafe()
        {
        }

        public static SingletonSimpleThreadSafe Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SingletonSimpleThreadSafe();
                    }
                    return instance;
                }
            }
        }
    }
}