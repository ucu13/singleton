﻿
namespace Singleton.SoldClient
{
    public sealed class SoldClient
    {
        private decimal Total = 0M;

        static SoldClient()
        {
        }

        private SoldClient()
        {
        }

        public static SoldClient Instance { get; } = new SoldClient();

        public decimal VizualizareSold()
        {
            return Total;
        }
        public void Adauga(decimal valoare)
        {
            Total += valoare;
        }
        public void Retrage(decimal valoare)
        {
            Total -= valoare;
        }
    }
}