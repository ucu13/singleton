﻿using System.Threading.Tasks;

namespace Singleton.SoldClient
{
    public class ProcesarePlata
    {
        public void AdaugaIncasari(decimal[] incasari)
        {
            Parallel.ForEach(incasari, valoare =>
            {
                SoldClient.Instance.Adauga(valoare);
            });
        }

        public void AdaugaCheltuieli(decimal[] plati)
        {
            Parallel.ForEach(plati, valoare =>
            {
                SoldClient.Instance.Retrage(valoare);
            });
        }
    }
}
