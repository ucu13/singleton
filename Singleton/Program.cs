﻿using Singleton.SoldClient;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Singleton
{
    public class Program
    {
        public static void Main(string[] args)
        {
            decimal[] incasari = { 39.99m, 29.99m, 19.99m, 49.99m , 4m, 67m};
            decimal[] plati = { 0.96m, 10m, 10.96m };

            Console.WriteLine("Sold Initial : " + SoldClient.SoldClient.Instance.VizualizareSold());

            ProcesarePlata process1 = new ProcesarePlata();
            ProcesarePlata process2 = new ProcesarePlata();

            var tasks = new[]
            {
                Task.Factory.StartNew(() => process1.AdaugaIncasari(incasari)),
                Task.Factory.StartNew(() => process2.AdaugaIncasari(incasari)),
                Task.Factory.StartNew(() => process1.AdaugaCheltuieli(plati))
            };

            Task.WaitAll(tasks);

            Console.WriteLine("Sold Final : " + SoldClient.SoldClient.Instance.VizualizareSold());

            Console.ReadLine();
        }
    }
}